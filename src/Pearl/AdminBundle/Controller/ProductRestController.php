<?php

namespace Pearl\AdminBundle\Controller;

use Pearl\AdminBundle\Entity\Product;
use Pearl\AdminBundle\Pagination\PaginatedCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use FOS\RestBundle\Controller\Annotations as Rest; 
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Swagger\Annotations as SWG;
use Pearl\AdminBundle\Controller\BaseController;


class ProductRestController extends BaseController
{

    /**
     * Get List of product
     * @return array
     * 
     * 
     * 
     * @ApiDoc(
     *    description="Récupère la liste des produits"
     * )
     *
     * @Rest\View(serializerGroups={"product"})
     * @Rest\Get("/products")
     */

    public function getProductsAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $em = $this->getDoctrine()->getManager();
        $qb = $this->getDoctrine()
        ->getRepository('PearlAdminBundle:Product')
        ->findAllQueryBuilder();

        $adapter = new DoctrineORMAdapter($qb);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($page);
        $products = [];
        foreach ($pagerfanta->getCurrentPageResults() as $result) {
            $products[] = $result;
        }

        //$products = $em->getRepository('PearlAdminBundle:Product')->findAll();
        $paginatedCollection = new PaginatedCollection($products, $pagerfanta->getNbResults());
        
        $route = 'get_products';
        $routeParams = array();
        $createLinkUrl = function($targetPage) use ($route, $routeParams) {
            return $this->generateUrl($route, array_merge(
                $routeParams,
                array('page' => $targetPage)
            ));
        };
        $paginatedCollection->addLink('self', $createLinkUrl($page));
        $paginatedCollection->addLink('first', $createLinkUrl(1));
        $paginatedCollection->addLink('last', $createLinkUrl($pagerfanta->getNbPages()));
        if ($pagerfanta->hasPreviousPage()) {
            $paginatedCollection->addLink('prev', $createLinkUrl($pagerfanta->getPreviousPage()));
        }


        $response = $this->createApiResponse($paginatedCollection, 200);
        return $response;
    }

    /**
    * Get single product 
    * @param Product $product 
    * @return array
    * @view()
    * @ParamConverter("product", class="PearlAdminBundle:Product")
    
     * @ApiDoc(
     *    description="Récupère un produit"
     * )
     */
    public function getProductAction(Product $product)
    {
        return compact('product');
    }    
   

}