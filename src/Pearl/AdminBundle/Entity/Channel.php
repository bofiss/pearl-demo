<?php

namespace Pearl\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * channel
 *
 * @ORM\Table(name="channel")
 * @ORM\Entity(repositoryClass="Pearl\AdminBundle\Repository\channelRepository")
 */
class Channel
{
   
    /**
     * @ORM\ManyToOne(targetEntity="Pearl\AdminBundle\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return channel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    

    public function setProduct(Product $product)
    {
      $this->product = $product;
      return $this;
    }

    /**
     * Get channel
     *
     * @return float 
     */
    public function getProduct()
    {
        return $this->product;
    }

}
