<?php

namespace Pearl\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Pearl\AdminBundle\Entity\Product;

class LoadProductsData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i< 40; $i++) {
           $product = new Product();
           $product->setNom("product" . $i);
           $product->setDescription('my new product with id of: '.$i);
           $product->setPrice(100 + $i);
           $product->setPictureUrl('http://localhost:8000/img/product'.$i.'.jpg');
           $manager->persist($product);
           $manager->flush();
        }
        
    }
}